﻿using de_serialize;
using System.Diagnostics;
using System.Text;
using System.Text.Json;

var timer = new Stopwatch();
var iterations = 10000;
var fClassObject = F.Get();
Serializer serializer = new();

timer.Start();
//Для справедливого подсчета без сохранения результата
for(int i=0; i < iterations; i++)
{
    serializer.SerializeCustom(fClassObject);
}
timer.Stop();
var serTimeCustom = timer.ElapsedMilliseconds;

var serialized = new StringBuilder();
//Выполняем для вывода потом в консоль
for (int i = 0; i < iterations; i++)
{
    serialized.Append(serializer.SerializeCustom(fClassObject));
}

timer.Restart();
Console.WriteLine(serialized.ToString());
timer.Stop();
var outputTime = timer.ElapsedMilliseconds;

timer.Restart();
for (int i = 0; i < iterations; i++)
{
    JsonSerializer.Serialize(fClassObject, new JsonSerializerOptions { IncludeFields = true });
}
timer.Stop();
var serTimeDotNet = timer.ElapsedMilliseconds;

timer.Restart();
for (int i =0; i< iterations; i++)
{
   serializer.DeserializeCustom("k1,1;k2,2;k3,3;k4,4;k5,5");
}
timer.Stop();
var deSerTimeCustom = timer.ElapsedMilliseconds;

timer.Restart();
for (int i = 0; i < iterations; i++)
{
    JsonSerializer.Deserialize<F>("{ \"i1\":1,\"i2\":2,\"i3\":3,\"i4\":4,\"i5\":5}");
}
timer.Stop();
var deSerTimeStandart = timer.ElapsedMilliseconds;
Console.WriteLine(@$"
    Сериализуемый класс: class F public int i1, i2, i3, i4, i5;
    код сериализации-десериализации: https://gitlab.com/robertorbbit/hw-reflection/-/blob/master/de-serialize
    количество замеров: {iterations} итераций
    мой рефлекшен:
    Время на сериализацию = {serTimeCustom}ms
    Время вывод на консоль: {outputTime}ms
    Время на десериализацию = {deSerTimeCustom} ms
    стандартный механизм: 
    Время на сериализацию = {serTimeDotNet} ms
    Время на десериализацию = {deSerTimeStandart} ms"
    );