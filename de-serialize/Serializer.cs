﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace de_serialize
{
    public class Serializer
    {
        public string SerializeCustom(object objToSerialize)
        {
            var serialized = new StringBuilder();
            var type = objToSerialize.GetType();
            var fields = type.GetFields();
            serialized.Append("{");
            foreach (var f in fields)
            {
                var name = f.Name;
                var value = f.GetValue(objToSerialize)?.ToString();
                serialized.Append($"\"{name}\":{value},");
            }
            serialized[serialized.Length-1] = '\0';
            serialized.Append("}");
            return serialized.ToString();
        }

        public object DeserializeCustom(string instance)
        {
            var splitedIn = instance.Split(';');
            var dynObject = new ExpandoObject();
            foreach (var s in splitedIn)
            {
                var nameValue = s.Split(',');
                var name = nameValue[0];
                var value = nameValue[1];
                dynObject.TryAdd(name, value);

            }
            return dynObject;
        }
    }
}
